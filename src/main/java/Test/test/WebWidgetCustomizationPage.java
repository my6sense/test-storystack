/**
 * 
 */
package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Ayelet
 *
 */
public class WebWidgetCustomizationPage extends AccountPage {

	
	public enum Layout {
		CLASSIC ("Classic"),
		HIDDEN ("Hidden"),
		FLOATING ("Floating"),
		TEXT_ONLY ("Text Only"),
		VERTICAL ("Vertical");
		
		private final String str; 
		
		Layout(String msg) {
			str=msg;
		}
		public String toString() {
			return str;
		}
	}
	
	public static final String NO_EXTERNALS="None";
	
	public static final By externalsLocator = new By.ByXPath("//select[@ng-model=\"contentBar.widgetSettings.externalItemsNum\"]");
	public static final By layoutLocator = new By.ByXPath("//select[@ng-model=\"contentBar.widgetSettings.layout\"]");
	public static final By titleLocator = new By.ByXPath("//input[@ng-model=\"contentBar.widgetSettings.title\"]");
	public static final By saveButtonLocator = new By.ByXPath("//button[text()=\"Save and Get Code\"]");
	
	public static final By previewFrameLocator = new By.ById("content-bar-iframe-my6");
	public static final By previewMainTitleLocator = new By.ById("my6_mainTitle");
	
	/**
	 * @param driver
	 */
	public WebWidgetCustomizationPage(WebDriver driver) {
		super(driver);
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(previewFrameLocator));
		PageFactory.initElements(driver,this);
	}

	public WebWidgetCustomizationPage setLayout (Layout layout) {
		WebDriverManager.selectElementByText(layoutLocator, layout.toString());
		return this;
	} 
	
	// Will throw NoSuchElementException if run outside range of external items
	public WebWidgetCustomizationPage setExternals(int externals) {
		String s;
		if (externals ==0) {
			s= NO_EXTERNALS;
		}
		else {
			s = new Integer(externals).toString();
		}
		WebDriverManager.selectElementByText(externalsLocator, s);
		return this;
	}

	public WebWidgetCustomizationPage setTitle(String title) {
		WebDriverManager.typeElement(titleLocator, title);
		return this;
	}
	
	public WebWidgetCustomizationPage checkPreviewTitle(String title){
		WebDriverWait wait = WebDriverManager.getWait();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(previewFrameLocator));
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(previewMainTitleLocator));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(previewMainTitleLocator, title));
		driver.switchTo().defaultContent();
		return this;
	}
	
	public AdminAppPage clickSave(){
		WebDriverManager.clickElement(saveButtonLocator);
		return PageFactory.initElements(driver, AdminAppPage.class);
	}
	
	
}
