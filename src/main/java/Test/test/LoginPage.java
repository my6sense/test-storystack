package Test.test;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	
	public static final String CONGRATS = "Congratulations, your account has been verified!";
	public enum Error {
		LOGIN_FAIL ("Login failed"),
		VERIFICATION_NEEDED ("This account has yet to be verified");
		private final String message; 
		Error(String msg) {
			message=msg;
		}
		public String message() {
			return message;
		}
	}
	
	private WebDriver driver;

	
//	always on login page
	public static final By emailLocator = new By.ById("email");
	public static final By passwordLocator = new By.ById("password");
	public static final By loginLocator = new By.ByXPath("//button[contains(@class,'login')]"); 
	public static final By signupLocator = new By.ByLinkText("Signup in less than 30 seconds.");
	public static final By createButtonLocator = new By.ByXPath("//div[contains(@class,\"btn\") and text()=\"Signup\"]");
	
	//sometimes on login page
	public static final By notificationLocator = new By.ByXPath("//*[@id=\"notifications-container-my6\"]//div[contains(@class,'message')]" );
	public static final By h3Locator = new By.ByXPath("//h3[@class=\"ng-binding\"]");
	public static final By okButtonLocator = new By.ByXPath("//button[text()=\"OK\"]");	
	
	public LoginPage(WebDriver driver) {
		super();
		this.driver = driver;
		
		WebDriverWait wait= WebDriverManager.getWait();
		ExpectedCondition<WebElement> cond = ExpectedConditions.presenceOfElementLocated(loginLocator);
		wait.until(cond);
		
		PageFactory.initElements(driver, this);

		//	Check that we're on the right page. Note there is some redundency here
		cond = ExpectedConditions.visibilityOfElementLocated(loginLocator);
		wait.until(cond);
		
		if (!driver.findElement(loginLocator).isDisplayed()) {
			throw new IllegalStateException("This is not the login page");
		}
	}
	
	// The login page allows the user to type their username into the username field
	public LoginPage typeEmail(String username) {
		WebDriverManager.typeElement(emailLocator, username);
		return this;    
	}

	public LoginPage typePassword(String pass) {
		WebDriverManager.typeElement(passwordLocator,pass);
		return this;    
	}

	

	
	private void login(String username, String pass) {
		typeEmail(username);
		typePassword(pass);
		WebDriverManager.clickElement(loginLocator);
	}

	public AdminAppPage loginAs(String username, String pass) {
		login(username, pass);
		return PageFactory.initElements(driver, AdminAppPage.class);
	}

	public AccountPage loginAsAccount(String username, String pass) {
		login(username, pass);
		return PageFactory.initElements(driver, AccountPage.class);
	}
	
	public LoginPage loginAsExpectFail(String username, String pass, Error err) {
		login(username, pass);
		// this will - in theory at least - throw "TimeoutException" if the notification did not appear.
		WebDriverManager.getWait().until(ExpectedConditions.textToBePresentInElementLocated(notificationLocator, err.message()));
		return this;
	}

	public CreateAccountPage createNew(){
		WebDriverManager.clickElement(createButtonLocator);
		return PageFactory.initElements(driver, CreateAccountPage.class);
	}
	
	public CreateAccountPage createNewSignup(){
		WebDriverManager.clickElement(signupLocator);
		return PageFactory.initElements(driver, CreateAccountPage.class);
	}
	
	public LoginPage verified() {
		WebDriverWait wait =WebDriverManager.getWait(); 
		wait.until(ExpectedConditions.presenceOfElementLocated(h3Locator));
//		wait.until(ExpectedConditions.textToBePresentInElementLocated(h3Locator, CONGRATS));
		
		wait.until(ExpectedConditions.presenceOfElementLocated(okButtonLocator));
		WebDriverManager.clickElement(okButtonLocator);
		
		// for sensitive click events, use this to make sure login page got loaded
		wait.until(ExpectedConditions.presenceOfElementLocated(LoginPage.loginLocator));
		return this;
	}
}
