package Test.test;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverManager {
	
	private static WebDriver d=null;
	
	// not sure if this bit is needed, and when. but I already wrote it in so let it stay for now.
	private static WebDriverWait wait;
	
	private static final int timeoutInSeconds = 7;
	private static final int certFailTimeoutInSeconds = 1;
	
	// This is here because I didn't have any ideas for a better location,
	// not because it inherently belongs in the webDriver class
	public static final String TEST_URL ="https://test.my6sense.com/";
	// common entry points
	public static final String CREATE_URL = TEST_URL + "CreateAccount/1";
	public static final String LOGIN_URL = TEST_URL + "/Login";
	public static final int ANIMATION_WAIT=1000; //wait for modal animations to load (ms) 
	
	public static final By overrideLocator = new By.ById("overridelink"); 
	
	public static WebDriver getDriverInstance() {
		return d;
	}
	
	private static WebDriver startIEDriver() {
		File file = new File("C:\\workspace\\IEDriver32\\IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		// only use this when I can't use the ie settings to configure the security zones 
	//	capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);
		return new InternetExplorerDriver(capabilities);
	}

	public static WebDriver startDriver() {
		d = new FirefoxDriver();
	//	d= startIEDriver();
	//	d = new ChromeDriver();
		d.manage().timeouts().implicitlyWait(timeoutInSeconds, TimeUnit.SECONDS);
		wait = new WebDriverWait(d, timeoutInSeconds);
		return d;
	}

	public static void stopDriver() {
		d.close();
		d.quit();
	}
	
	public static void overrideCertificate() {
		try {
			WebDriverWait wait1 = new WebDriverWait(d, certFailTimeoutInSeconds);
			wait1.until(ExpectedConditions.presenceOfElementLocated(overrideLocator));
			wait1.until(ExpectedConditions.elementToBeClickable(overrideLocator));
			d.findElement(overrideLocator).click();
		}
		catch(TimeoutException t)	{
		 //if the element is not found, well, great :)
		}
	}
	
	public static WebDriverWait getWait(){
		return wait;
	}
	
	public static void clickElement(By locator){
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		d.findElement(locator).click();
	}
	
	public static void typeElement(By locator, String text){
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		d.findElement(locator).clear();
		d.findElement(locator).sendKeys(text);
	}
	
	public static void selectElementByText(By locator, String text){
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		Select droplist = new Select(d.findElement(locator));   
		droplist.selectByVisibleText(text);
	}
}