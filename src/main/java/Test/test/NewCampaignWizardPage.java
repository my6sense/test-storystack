package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NewCampaignWizardPage extends AccountPage {

	public static final By cmpRssLocator = new By.ById("campaignRSS_new");
	public static final By nextButtonLocator = new By.ByXPath("//button[text()=\"Next\"]");

	
	public NewCampaignWizardPage(WebDriver driver) {
		super(driver);
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(cmpRssLocator));
		PageFactory.initElements(driver,this);
	}
	
	public NewCampaignWizardPage typeRss(String rss) {
		WebDriverManager.typeElement(cmpRssLocator, rss);
		return this;
	}
	
	public boolean isNextEnabled(){
		return driver.findElement(nextButtonLocator).isEnabled();
	}
	
	public CampaignSettingsPage clickNext(){
		WebDriverManager.clickElement(nextButtonLocator);
		return PageFactory.initElements(driver,CampaignSettingsPage.class);
	}
	
}
