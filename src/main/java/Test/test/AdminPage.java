package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AdminPage extends AdminAppPage {

	public static final By newAccountButtonLocator = new By.ByXPath("//a[@ng-click=\"openCloseNewAccountForm()\"]");

	
	public AdminPage(WebDriver driver) {
		super(driver);
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(newAccountButtonLocator));
		WebDriverManager.getWait().until(ExpectedConditions.elementToBeClickable(newAccountButtonLocator));
	}

}
