package Test.test;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class CreateAccountPage {

	public static final String THANK_YOU= "Thank you for registering for Storystack!";
	
	private WebDriver driver;

	public static final By loginButtonLocator = new By.ByXPath("//div[contains(@class,\"btn\") and text()=\"Login\"]");
	public static final By accountNameLocator = new By.ById("accountName");
	public static final By firstNameLocator = new By.ById("firstName");
	public static final By lastNameLocator = new By.ById("lastName");
	public static final By emailLocator = new By.ById("email");
	public static final By passwordLocator = new By.ById("password");
	public static final By passwordRepeatLocator = new By.ById("passwordRepeat");
	public static final By phoneNumberLocator = new By.ById("phoneNumber");
	public static final By accountUrlLocator = new By.ById("accountUrl");
	
//	public static final By personaLocator = new By.ById("persona");
//	public static final By marketSegmentSelectLocator = new By.ById("marketSegmentSelect");
//	public static final By siteTrafficLocator = new By.ById("siteTraffic");
//	public static final By GMTtimeZoneSelectLocator = new By.ById("GMTtimeZoneSelect");
	public static final By nextButtonLocator = new By.ById("loginNextButton");
	
//	@FindBy(xpath="//h3[text()=\"" + THANK_YOU +"\")]")
//	private WebElement _h3;
	
//	@FindBy(xpath="//button[@ng-click=\"$close(btn.result)\"]")
	@FindBy(xpath="//button[text()=\"OK\"]")
	private WebElement _okButton;

	public CreateAccountPage(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);

		if (!(driver.getCurrentUrl().contains("CreateAccount"))){
			throw new IllegalStateException("This is not the create account page");
		}
	}

	public LoginPage login() {
		WebDriverManager.clickElement(loginButtonLocator);
		return PageFactory.initElements(driver, LoginPage.class);
	}

	public CreateAccountPage typeAccountName(String text) {
		WebDriverManager.typeElement(accountNameLocator,text);
		return this;
	}

	public CreateAccountPage typeFirstName(String text) {
		WebDriverManager.typeElement(firstNameLocator,text);
		return this;
	}

	public CreateAccountPage typeLastName(String text) {
		WebDriverManager.typeElement(lastNameLocator,text);
		return this;
	}

	public CreateAccountPage typeEmail(String text) {
		WebDriverManager.typeElement(emailLocator,text);
		return this;
	}

	public CreateAccountPage typePassword(String text) {
		WebDriverManager.typeElement(passwordLocator,text);
		return this;
	}

	public CreateAccountPage typePasswordRepeat(String text) {
		WebDriverManager.typeElement(passwordRepeatLocator,text);
		return this;
	}

	public CreateAccountPage typePhone(String text) {
		WebDriverManager.typeElement(phoneNumberLocator,text);
		return this;
	}

	public CreateAccountPage typeUrl(String text) {
		WebDriverManager.typeElement(accountUrlLocator,text);
		return this;
	}

//	public CreateAccountPage chooseMarketSement(String value) {
//		Select droplist = new Select(driver.findElement(marketSegmentSelectLocator));   
//		droplist.selectByVisibleText(value);
//		return this;
//	}

//	public CreateAccountPage chooseTraffic(String value) {
//		Select droplist = new Select(driver.findElement(siteTrafficLocator));   
//		droplist.selectByVisibleText(value);
//		return this;
//	}
//
//	public CreateAccountPage chooseTimezone(String value) {
//		Select droplist = new Select(driver.findElement(GMTtimeZoneSelectLocator));   
//		droplist.selectByVisibleText(value);
//		return this;
//	}

	public CreateAccountStep2Page next(){
		WebDriverManager.clickElement(nextButtonLocator);
		return PageFactory.initElements(driver, CreateAccountStep2Page.class);
	}
//	/**
//	 * use this function after the "submit" button was clicked - verify the message and press "ok"
//	 * @return the login page that is the click result. 
//	 */
//	public LoginPage validateEmailSent(){
////		WebDriverManager.getWait().until(ExpectedConditions.visibilityOf(_h3));
////		Assert.assertTrue(_h3.getText().contains(THANK_YOU));
//		WebDriverManager.getWait().until(ExpectedConditions.visibilityOf(_okButton));
//		WebDriverManager.getWait().until(ExpectedConditions.elementToBeClickable(_okButton));
//		_okButton.click();
//		// for sensitive click events, use this to make sure login page got loaded
//		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(LoginPage.loginLocator));
//		return PageFactory.initElements(driver, LoginPage.class);
//		
//	}

	/** 
	 * Types all the mandatory text fields
	 * @param org
	 * @param first
	 * @param last
	 * @param username
	 * @param pass
	 * @param repass
	 * @param phone
	 * @param accountUrl
	 * @return the current page
	 */
	public CreateAccountPage typeMandatory(String org, String first, String last, String username, String pass,String repass, String phone, String accountUrl)
	{
		typeAccountName(org);
		typeFirstName(first);
		typeLastName(last);
		typeEmail(username);
		typePassword(pass);
		typePasswordRepeat(repass);
		typePhone(phone);
		typeUrl(accountUrl);
		return this;
	}

	/**
	 * Enter all the inputs needed to start a new account
	 * @param org
	 * @param first
	 * @param last
	 * @param username
	 * @param pass
	 * @param repass
	 * @param phone
	 * @param accountUrl
	 * @return the current page
	 */
	public CreateAccountPage inputAll(String org, String first, String last, String username, String pass,String repass, String phone, String accountUrl,String accountType,String market, String traffic, String timezone) {
		typeMandatory(org, first, last, username, pass, repass, phone, accountUrl);
		return this;
	}

//	public LoginPage createAccount(String org, String first, String last, String username, String pass,String repass, String phone, String accountUrl,String accountType,String market, String traffic, String timezone) {
//		inputAll(org, first, last, username, pass, repass, phone, accountUrl, accountType, market, traffic, timezone);
//		submit();
//		PageFactory.initElements(driver,this);
//		return validateEmailSent();
//	}
}
