package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CampaignSetBudgetPage extends AccountPage {

	public static final By cpcLocator = new By.ById("creditsPerClick_new");
	public static final By dailyCapLocator = new By.ById("dailyCap_new");
	public static final By budgetLocator = new By.ById("budget_new");
	
	public static final By doneButtonLocator = new By.ByXPath("//button[text()=\"Done\"]");

	

	public CampaignSetBudgetPage(WebDriver driver) {
		super(driver);
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(cpcLocator));
		PageFactory.initElements(driver,this);
	}

	
	public CampaignSetBudgetPage typeCpc(String value) {
		WebDriverManager.typeElement(cpcLocator, value);
		return this;
	}
	
	public CampaignSetBudgetPage typeDailyCap(String value) {
		WebDriverManager.typeElement(dailyCapLocator, value);
		return this;
	}
	
	public CampaignSetBudgetPage typeBudget(String value) {
		WebDriverManager.typeElement(budgetLocator, value);
		return this;
	}
	
	public boolean isDoneEnabled(){
		return driver.findElement(doneButtonLocator).isEnabled();
	}
	
	public AccountPage clickDone(){
		WebDriverManager.clickElement(doneButtonLocator);
		return PageFactory.initElements(driver,AccountPage.class);
	}
}
