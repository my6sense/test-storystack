package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AdminAppPage {

	protected WebDriver driver;
	
	public static final By logoutLocator = new By.ByLinkText("Logout");
	@FindBy(linkText = "Logout")
	private WebElement logoutButton;
	
	public AdminAppPage(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver,this);
		//will throw timeout exception if the logout link is missing
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(logoutLocator));
	}
	
	public LoginPage logout() {
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(logoutLocator));
		WebDriverManager.clickElement(logoutLocator);

		return PageFactory.initElements(driver, LoginPage.class);
	}
}
