package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class WidgetsPage extends AccountPage {

	public static final By newWidgetLocator = new By.ByLinkText("New Widget");
	
	public WidgetsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public NewWidgetPage clickNewWidget(){
		WebDriverManager.clickElement(newWidgetLocator);
		return PageFactory.initElements(driver, NewWidgetPage.class);
	}

}
