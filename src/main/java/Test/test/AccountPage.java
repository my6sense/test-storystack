/**
 * 
 */
package Test.test;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Ayelet
 *
 */
public class AccountPage extends AdminAppPage {

	// driver is inherited, also logout button
	
	public static final By campaignsLocator = new By.ByLinkText("Campaigns");
	public static final By widgetsLocator = new By.ByLinkText("Widgets");
	public static final By reportsLocator = new By.ByLinkText("Reports");
	
	/**
	 * @param driver
	 */
	public AccountPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver,this);
	}
	
	/** 
	 * click the "Campaigns" link
	 * @return
	 */
	public AccountPage campaigns(){
		WebDriverManager.clickElement(campaignsLocator);
		return this;
	}
	
	public AccountPage widgets(){
		WebDriverManager.clickElement(widgetsLocator);
		// try again if we did not reach the widgets page, trying to stabilize the code
		if (!(driver.getCurrentUrl().contains("Widgets"))) {
			WebDriverManager.clickElement(widgetsLocator);	
		}
		return this;
	}
	
	public AccountPage reports(){
		WebDriverManager.clickElement(reportsLocator);
		return this;
	}

}
