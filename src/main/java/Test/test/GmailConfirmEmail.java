package Test.test;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.search.RecipientTerm;
import javax.mail.search.SearchTerm;

import junit.framework.Assert;


public class GmailConfirmEmail {
	
	private static final String gmailUrl="https://mail.google.com/";
	private static final String email="test@my6sense.com";
	private static final String pass="my6sense";
	
	private static final String textBefore="https://";
	private static final String textAfter="\r\n";

	private static final int waitForMessageSeconds = 60;
	
	/**
	 * Go to the gmail login page, find the confirmation email and click it
	 * @param address the email address of the email to confirm.
	 */
	public static String getVerificationLink(String address) throws Exception {
		 Properties props = new Properties();
	        props.setProperty("mail.store.protocol", "imaps");
	        try {
	        	// connect to gmail account
	            Session session = Session.getInstance(props, null);
	            Store store = session.getStore();
	            store.connect("imap.gmail.com", email, pass);
	            Folder inbox = store.getFolder("INBOX");
	            inbox.open(Folder.READ_ONLY);
	            
	            // get the relevant message based on TO field
	            SearchTerm sTerm = new RecipientTerm(Message.RecipientType.TO,new InternetAddress(address));
	            Message messages[] = inbox.search(sTerm);
	            
	            for (int i=0; i<waitForMessageSeconds;i++) {
	            	if (messages.length==1) break;
	            	Thread.sleep(1000);
	            	inbox.close(false);
	            	inbox.open(Folder.READ_ONLY);
	            	messages = inbox.search(sTerm);
	            }
	            Assert.assertTrue("found " + messages.length + " messages instead of 1 expected", messages.length==1);
	            Message msg = messages[0];
	            String content = (String)msg.getContent();
	            
	            // get the link from the message content
	            int startAt = content.indexOf(textBefore);
	            int endAt = content.indexOf(textAfter, startAt);
	            return content.substring(startAt, endAt);

	        } catch (Exception mex) {
	            mex.printStackTrace();
	            throw mex;
	        }
	}

}
