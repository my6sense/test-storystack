package Test.test;

public enum AccountType {
	PUBLISHER ("Publisher"),
	ADVERTISER ("Advertiser"),
	BOTH("Pub + Ad");
	private final String str; 
	AccountType(String msg) {
		str=msg;
	}
	public String toString() {
		return str;
	}
}
