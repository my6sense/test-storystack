package Test.test;

import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Hello world!
 *
 */
public class App 
{
	private WebDriver driver;
	
	private static final String org="Automation Test Org";
	private static final String first="Automated";
	private static final String last="Test";
	private static final String emailPre="test+auto";
	private static final String emailPost="@my6sense.com";
	private static final String pass="123123";
	private static final String phone="999-9999";
	private static final String url = "http://my6sense.com";
	private static final String marketSegment="Parenting & Baby";
	private static final String traffic = "10k-100k";
	private static final String tz = "(GMT+02:00) Jerusalem";
	
	public App(WebDriver d){
		driver = d;
	}
	
	public CreateAccountPage gotoCreate() {
		driver.get(WebDriverManager.CREATE_URL);
		WebDriverManager.overrideCertificate();
		return PageFactory.initElements(driver, CreateAccountPage.class);	
	}
	
	/** 
	 * This function creates a new user, using the sign-up page.
	 * This version receives one parameter for account type and creates a default new account. 
	 * returns the username (=email) of newly created user.
	 */
	public String createNewUser(AccountType accountType) throws Exception {
		CreateAccountPage page = gotoCreate();
		Date now = new Date();
		String email = emailPre + now.getTime() + emailPost;
		page.typeMandatory(org, first, last, email, pass, pass, phone, url);
		CreateAccountStep2Page p2 = page.next();
		p2.createAccount(accountType, marketSegment, traffic, tz);
		String link= GmailConfirmEmail.getVerificationLink(email);
		driver.get(link);
		LoginPage login = PageFactory.initElements(driver, LoginPage.class);
		login.verified();
		
		return email;
	}
	
	/**
	 * Create a new user of type BOTH, return email
	 * @return generated account's email
	 * @throws Exception
	 */
	public String createNewUser() throws Exception {
		return createNewUser(AccountType.BOTH);
	}
	
	public String createWidget(String account,String url, String cbName) {
		driver.get(WebDriverManager.TEST_URL + "/Login");
		WebDriverManager.overrideCertificate();
		LoginPage login = PageFactory.initElements(driver, LoginPage.class);
		AccountPage accp= login.loginAsAccount(account,pass);
		accp.widgets();
		try {
			WidgetsPage wp = PageFactory.initElements(driver, WidgetsPage.class);
			wp.clickNewWidget();
		}
		catch (TimeoutException e){
			// if the button is not found, we're likely at the wiszard already
		}
		//either way do..
		NewWidgetPage page = PageFactory.initElements(driver, NewWidgetPage.class);
		page.typeName(cbName);
		page.setAgeRestriction(NewWidgetPage.AgeRestriction.AGE_0);
		page.confirmAgeRestriction();
		page.setRss(url);
		Assert.assertTrue("Next button should be enabled!", page.isNextEnabled());
		page.clickNext();
		WebWidgetCustomizationPage cp = PageFactory.initElements(driver, WebWidgetCustomizationPage.class);
		//cp.checkPreviewTitle("You may also like");
		cp.clickSave();
		return "success!";
	}
	
	public String createWidget(String account,String url) {
		return createWidget(account,url,org);
	}
}
