package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NewWidgetPage extends AccountPage {
	
	public enum Media {
		WEB ("Web"),
		MOBILE ("Mobile");
		private final String str; 
		Media(String msg) {
			str=msg;
		}
		public String toString() {
			return str;
		}
	}
	
	public enum AgeRestriction {
		AGE_0(ageNoneLocator),
		AGE_13(age13Locator),
		AGE_18(age18Locator);
		private final By locator;
		AgeRestriction(By locator){
			this.locator = locator;
		}
		public By getLocator() {
			return locator;
		}
	}
	public static final By cbNameLocator = new By.ById("contentBarNameInput_new");
	
	public static final By enableLocator = new By.ByXPath("//button[contains(text(),\"Enable\")]");
	public static final By disableLocator = new By.ByXPath("//button[contains(text(),\"Disable\")]");

	public static final By mediaLocator = new By.ById("contentBarClientTypeSelect_new");

	public static final By cbRssRadioLocator = new By.ByXPath("//label[input[@id=\"contentBarRSS\"]]");
	public static final By cbUrlRadioLocator = new By.ByXPath("//label[input[@id=\"rssURL\"]]");
	public static final By cbRssLocator = new By.ById("contentBarRSS_new");
	public static final By cbUrlLocator = new By.ById("contentBarURL_new");

	public static final By ageNoneLocator = new By.ByXPath("//label[input[@id=\"restrictedRadio0_new_contentBar\"]]");
	public static final By age13Locator = new By.ByXPath("//label[input[@id=\"restrictedRadio1_new_contentBar\"]]");
	public static final By age18Locator = new By.ByXPath("//label[input[@id=\"restrictedRadio2_new_contentBar\"]]");
	public static final By ageCheckLocator = new By.ByXPath("//label[input[@id=\"contentDeclarationCheck_new_contentBar\"]]");
	
	public static final By nextButtonLocator = new By.ByXPath("//button[text()=\"Next\"]");
	
	public static final By blackListLocator = new By.ByName("contentBar_blacklist");
	
	public NewWidgetPage(WebDriver driver) {
		super(driver);
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(cbNameLocator));
		PageFactory.initElements(driver,this);
	}
	
	public NewWidgetPage typeName(String name) {
		WebDriverManager.typeElement(cbNameLocator, name);
		return this;
	}
	
	public NewWidgetPage enable() {
		WebDriverManager.clickElement(enableLocator);
		return this;
	}
	
	public NewWidgetPage disable() {
		WebDriverManager.clickElement(disableLocator);
		return this;
	}
	
	public NewWidgetPage setMedia(Media m){
		WebDriverManager.selectElementByText(mediaLocator, m.toString());
		return this;
	}
	
	public NewWidgetPage setAgeRestriction(AgeRestriction age) {
		WebDriverManager.clickElement(age.getLocator());
		return this;
	}
	
	public NewWidgetPage confirmAgeRestriction() {
		WebDriverManager.clickElement(ageCheckLocator);
		return this;
	}
	
	public NewWidgetPage setRss(String url){
		WebDriverManager.clickElement(cbRssRadioLocator);
		WebDriverManager.typeElement(cbRssLocator, url);
		return this;
	}
		
	public NewWidgetPage setUrl(String url){
		WebDriverManager.clickElement(cbUrlRadioLocator);
		WebDriverManager.typeElement(cbUrlLocator, url);
		return this;
	}

	public boolean isNextEnabled(){
		return driver.findElement(nextButtonLocator).isEnabled();
	}
	
	public AccountPage clickNext(){
		WebDriverManager.clickElement(nextButtonLocator);
		return PageFactory.initElements(driver,AccountPage.class);
	}
}
