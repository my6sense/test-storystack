package Test.test;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebDriver;

import Test.test.NewWidgetPage.AgeRestriction;

public class CampaignSettingsPage extends AccountPage {
	
	public enum AgeRestriction {
		AGE_0(ageNoneLocator),
		AGE_13(age13Locator),
		AGE_18(age18Locator);
		private final By locator;
		AgeRestriction(By locator){
			this.locator = locator;
		}
		public By getLocator() {
			return locator;
		}
	}

	public static final By ageNoneLocator = new By.ByXPath("//label[input[@id=\"restrictedRadio0_newCampaign\"]]");
	public static final By age13Locator = new By.ByXPath("//label[input[@id=\"restrictedRadio1_newCampaign\"]]");
	public static final By age18Locator = new By.ByXPath("//label[input[@id=\"restrictedRadio2_newCampaign\"]]");
	public static final By ageCheckLocator = new By.ByXPath("//label[input[@id=\"contentDeclarationCheck_newCampaign\"]]");
	
	
	public static final By nextButtonLocator = new By.ByXPath("//button[text()=\"Next\"]");
	public static final By nameLocator = new By.ById("campaignName_new");
	
	public CampaignSettingsPage(WebDriver driver) {
		super(driver);
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(nameLocator));
		PageFactory.initElements(driver,this);
	}

	public CampaignSettingsPage typeName(String name) {
		WebDriverManager.typeElement(nameLocator, name);
		return this;
	}
	
	public boolean isNextEnabled(){
		return driver.findElement(nextButtonLocator).isEnabled();
	}
	
	public CampaignSetBudgetPage clickNext(){
		WebDriverManager.clickElement(nextButtonLocator);
		return PageFactory.initElements(driver,CampaignSetBudgetPage.class);
	}
	
	public CampaignSettingsPage setAgeRestriction(AgeRestriction age) {
		WebDriverManager.clickElement(age.getLocator());
		return this;
	}
	
	public CampaignSettingsPage confirmAgeRestriction() {
		WebDriverManager.clickElement(ageCheckLocator);
		return this;
	}
	
}
