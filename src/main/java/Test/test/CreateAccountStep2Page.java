package Test.test;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import Test.test.CampaignSettingsPage.AgeRestriction;


public class CreateAccountStep2Page {

	
	
	public static final String THANK_YOU= "Thank you for registering for Storystack!";
	
	private WebDriver driver;

	public static final By loginButtonLocator = new By.ByXPath("//div[contains(@class,\"btn\") and text()=\"Login\"]");
	public static final By personaLocator = new By.ById("persona");
	public static final By marketSegmentSelectLocator = new By.ById("marketSegmentSelect");
	public static final By siteTrafficLocator = new By.ById("siteTraffic");
	public static final By GMTtimeZoneSelectLocator = new By.ById("GMTtimeZoneSelect");
	public static final By termsLocator = new By.ByXPath("//input[@type=\"checkbox\"]");
	public static final By createButtonLocator = new By.ById("loginCreateAccoutButton");
	
//	@FindBy(xpath="//h3[text()=\"" + THANK_YOU +"\")]")
//	private WebElement _h3;
	
//	@FindBy(xpath="//button[@ng-click=\"$close(btn.result)\"]")
	@FindBy(xpath="//button[text()=\"OK\"]")
	private WebElement _okButton;

	public CreateAccountStep2Page(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);

		if (!(driver.getCurrentUrl().contains("CreateAccount"))){
			throw new IllegalStateException("This is not the create account page");
		}
	}

	public LoginPage login() {
		WebDriverManager.clickElement(loginButtonLocator);
		return PageFactory.initElements(driver, LoginPage.class);
	}


	public CreateAccountStep2Page chooseAccountType(AccountType type) {
		WebDriverManager.selectElementByText(personaLocator, type.toString());
		return this;
	}
	
	public CreateAccountStep2Page chooseMarketSement(String value) {
		WebDriverManager.selectElementByText(marketSegmentSelectLocator,value);
		return this;
	}

	public CreateAccountStep2Page chooseTraffic(String value) {
		WebDriverManager.selectElementByText(siteTrafficLocator,value);
		return this;
	}

	public CreateAccountStep2Page chooseTimezone(String value) {
		WebDriverManager.selectElementByText(GMTtimeZoneSelectLocator,value);
		return this;
	}

	public CreateAccountStep2Page submit(){
		WebDriverManager.clickElement(createButtonLocator);
		return this;
	}
	
	public CreateAccountStep2Page agree(){
		WebDriverManager.clickElement(termsLocator);
		return this;
	}
	/**
	 * use this function after the "submit" button was clicked - verify the message and press "ok"
	 * @return the login page that is the click result. 
	 */
	public LoginPage validateEmailSent(){
//		WebDriverManager.getWait().until(ExpectedConditions.visibilityOf(_h3));
//		Assert.assertTrue(_h3.getText().contains(THANK_YOU));
		WebDriverManager.getWait().until(ExpectedConditions.visibilityOf(_okButton));
		WebDriverManager.getWait().until(ExpectedConditions.elementToBeClickable(_okButton));
		_okButton.click();
		// for sensitive click events, use this to make sure login page got loaded
		WebDriverManager.getWait().until(ExpectedConditions.presenceOfElementLocated(LoginPage.loginLocator));
		return PageFactory.initElements(driver, LoginPage.class);
		
	}


	/**
	 * Enter all the inputs needed to start a new account
	 * @param accountType
	 * @param market
	 * @param traffic
	 * @param timezone
	 * @return the current page
	 */
	public CreateAccountStep2Page inputAll(AccountType accountType,String market, String traffic, String timezone) {
		chooseAccountType(accountType);
		if (market!=null) chooseMarketSement(market);
		if (traffic!= null) chooseTraffic(traffic);
		if (timezone !=null) chooseTimezone(timezone);
		return this;
	}

	public LoginPage createAccount(AccountType accountType,String market, String traffic, String timezone) {
		inputAll(accountType, market, traffic, timezone);
		agree();
		submit();
		PageFactory.initElements(driver,this);
		return validateEmailSent();
	}
}
