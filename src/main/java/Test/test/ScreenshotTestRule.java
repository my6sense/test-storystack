package Test.test;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;
//import org.junit.rules.MethodRule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
//import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


class ScreenshotTestRule implements TestRule {
	private static WebDriver driver; 
	private static String screenshotsBase = "target/screen-reports/"; 
	

	public Statement apply(final Statement statement, final Description arg1) {
		return new Statement() {
			@Override
			public void evaluate() throws Throwable {
				try {
					statement.evaluate();
				} catch (Throwable t) {
					captureScreenshot(arg1.toString());
					throw t; // rethrow to allow the failure to be reported to JUnit
				}
			}

			public void captureScreenshot(String method) {
				try {
					driver = WebDriverManager.getDriverInstance();
					new File(screenshotsBase).mkdirs(); // Insure directory is there
					Date now = new Date();
					String fn = screenshotsBase + method + now.getTime() + ".png";
			//		FileOutputStream out = new FileOutputStream(fn);
//					WebDriver augmentedDriver = new Augmenter().augment(driver);
//					File source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
					File source = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(source, new File(fn)); 
		//			out.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
		//			out.close();
				} catch (Exception e) {
					// No need to crash the tests if the screenshot fails
					System.out.println(e);
				}
			}
		};
	}
}
