package Test.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginTest {

	private static WebDriver driver;
	private String adminEmail="ayelet+admin@my6sense.com";
	private String adminPass="123123";
	private String accountEmail="ayelet@my6sense.com";
	private String accountPass="123123";
	
	@Rule
    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule();

	@BeforeClass
	public static void setUp() throws Exception {
		driver = WebDriverManager.startDriver();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		WebDriverManager.stopDriver();
	}

	public LoginPage gotoLogin(){
		driver.get(WebDriverManager.TEST_URL + "/Login");
		WebDriverManager.overrideCertificate();
		return PageFactory.initElements(driver, LoginPage.class);
	}
	
	public void testLoginLogout(String email, String pass) {
		LoginPage loginPage = gotoLogin();
		AdminAppPage page= loginPage.loginAs(email,pass);
		loginPage = page.logout();
	}
	
	@Test
	public void adminLogin() {
		LoginPage loginPage = gotoLogin();
		AdminAppPage page= loginPage.loginAs(adminEmail,adminPass);
		AdminPage ap = PageFactory.initElements(driver, AdminPage.class);
		loginPage = ap.logout();
	}
	
	@Test
	public void accountLogin() {
		LoginPage loginPage = gotoLogin();
		AccountPage page= loginPage.loginAsAccount(accountEmail,accountPass);
		page = page.campaigns();
		page = page.widgets();
		page = page.reports();
		page = page.campaigns();
		loginPage = page.logout();
	}
	
//	@Test
	/** This test should fail to allow automation testing - see our behaviour on failed tests **/
	public void failedLogin() {
		testLoginLogout(accountEmail,"123456");
	}
	
	@Test
	public void loginFail() {
		LoginPage loginPage = gotoLogin();
		loginPage = loginPage.loginAsExpectFail(accountEmail,"123456", LoginPage.Error.LOGIN_FAIL);
	}
	
	@Test
	public void createAccount() {
		LoginPage login = gotoLogin();
		CreateAccountPage page = login.createNew();
		login = page.login();
	}
	
	@Test
	public void createAccountSignup() {
		LoginPage login = gotoLogin();
		CreateAccountPage page = login.createNewSignup();
		login = page.login();
	}

}
