package Test.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CreateAccountTest {

	private static WebDriver driver;
	
	private static final String org="Automation Test Org";
	private static final String first="Automated";
	private static final String last="Test";
	private static final String emailPre="test+auto";
	private static final String emailPost="@my6sense.com";
	private static String email;
	private static final String pass="123123";
	private static final String phone="999-9999";
	private static final String url = "http://my6sense.com";
	private static final String marketSegment="Parenting & Baby";
	private static final String traffic = "10k-100k";
	private static final String tz = "(GMT+02:00) Jerusalem";

	@Rule
    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule();

	@BeforeClass
	public static void setUp() throws Exception {
		driver = WebDriverManager.startDriver();
		//generate unique email for this test
		Date now = new Date();
		email = emailPre + now.getTime() + emailPost;
	}

	@AfterClass
	public static void tearDown() throws Exception {
		WebDriverManager.stopDriver();
	}

	public CreateAccountPage gotoCreate() {
		driver.get(WebDriverManager.CREATE_URL);
		WebDriverManager.overrideCertificate();
		return PageFactory.initElements(driver, CreateAccountPage.class);	
	}
	
	/**
	 * Create a new account; 
	 * try to login, expect "verification needed" error;
	 * verify the account by getting the link from email and clicking it;
	 * verify correct message;
	 * login successfully using the newly created account.
	 * @throws Exception
	 */
	@Test
	public void createNewAccount() throws Exception {
		// create a new account
		CreateAccountPage page = gotoCreate();
		page.typeMandatory(org, first, last, email, pass, pass, phone, url);
		CreateAccountStep2Page p2 = page.next();
		LoginPage login = p2.createAccount(AccountType.PUBLISHER, marketSegment, traffic, tz);

		// try to log in - expect fail
		login = PageFactory.initElements(driver,LoginPage.class);
		login.loginAsExpectFail(email, pass, LoginPage.Error.VERIFICATION_NEEDED);
		
		// verify email
		String link= GmailConfirmEmail.getVerificationLink(email);
		driver.get(link);
		
		// wait for load of login page
		login = PageFactory.initElements(driver, LoginPage.class);
		login.verified();
		// try to login - should succeed
		AdminAppPage aap = login.loginAs(email, pass);
		NewWidgetPage nwp = PageFactory.initElements(driver, NewWidgetPage.class);
		nwp.logout();
		// Great success!
	}
	
//	@Test
	public void easyCreateNewAccount() throws Exception {
		App app = new App(driver);
		String username = app.createNewUser();
		app.createWidget(username, url);
	}

}
