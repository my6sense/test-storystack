package Test.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class NewWidget {

	private static WebDriver driver;
	private static final String accountEmail="test+auto_new@my6sense.com";
	private static final String accountPass="123123";

	private static final String blogUrl ="http://mwwp.my6sense.com/wordpresses/wordpress2/?feed=rss2";
	
	private static final String cbName = "Automated Widget Name";
	
	@Rule
    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule();

	@BeforeClass
	public static void setUp() throws Exception {
		driver = WebDriverManager.startDriver();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		WebDriverManager.stopDriver();
	}

	@Test
	public void createWidget() {
		driver.get(WebDriverManager.TEST_URL + "/Login");
		WebDriverManager.overrideCertificate();
		LoginPage login = PageFactory.initElements(driver, LoginPage.class);
		login.loginAsAccount(accountEmail,accountPass);
		NewWidgetPage page= PageFactory.initElements(driver, NewWidgetPage.class);
		page.typeName(cbName);
		page.enable();
		page.disable();
		page.enable();
		page.setMedia(NewWidgetPage.Media.WEB);
		page.setMedia(NewWidgetPage.Media.MOBILE);
		page.setMedia(NewWidgetPage.Media.WEB);
		page.setAgeRestriction(NewWidgetPage.AgeRestriction.AGE_13);
		page.setAgeRestriction(NewWidgetPage.AgeRestriction.AGE_18);
		page.setAgeRestriction(NewWidgetPage.AgeRestriction.AGE_0);
		page.confirmAgeRestriction();
		Assert.assertFalse("Next button should not be enabled!", page.isNextEnabled());
		page.setRss(blogUrl);
		page.setUrl(blogUrl);
		page.setRss(blogUrl);
		Assert.assertTrue("Next button should be enabled!", page.isNextEnabled());
		page.clickNext();
		WebWidgetCustomizationPage cp = PageFactory.initElements(driver, WebWidgetCustomizationPage.class);
		cp.checkPreviewTitle("You may also like");
		cp.setExternals(0);
		cp.setLayout(WebWidgetCustomizationPage.Layout.FLOATING);
		cp.setTitle("Testing");
		cp.checkPreviewTitle("Testing");
		cp.setLayout(WebWidgetCustomizationPage.Layout.HIDDEN);
		cp.setExternals(5);
		cp.setTitle("Testing, 1");
		cp.checkPreviewTitle("Testing, 1");
		cp.setLayout(WebWidgetCustomizationPage.Layout.TEXT_ONLY);
		cp.setTitle("Testing, 1,2");
		cp.checkPreviewTitle("Testing, 1,2");
		cp.setLayout(WebWidgetCustomizationPage.Layout.VERTICAL);
		cp.setTitle("Testing, 1,2,3");
		cp.checkPreviewTitle("Testing, 1,2,3");
		cp.setLayout(WebWidgetCustomizationPage.Layout.CLASSIC);
		cp.setTitle("Testing, 1,2,3,4");
		cp.checkPreviewTitle("Testing, 1,2,3,4");
		cp.clickSave();
		
	}

}
