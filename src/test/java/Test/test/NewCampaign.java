/**
 * 
 */
package Test.test;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Ayelet
 *
 */
public class NewCampaign {

	private static WebDriver driver;
	private static final String accountEmail="ayelet+green@my6sense.com";
	private static final String accountPass="123123";

	private static final String campaignUrl ="http://mwwp.my6sense.com/wordpresses/wordpress1/?feed=rss2";
	
	private static final String cpName = "Automated Campaign Name";
	
	@Rule
    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule();

	@BeforeClass
	public static void setUp() throws Exception {
		driver = WebDriverManager.startDriver();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		WebDriverManager.stopDriver();
	}

	@Test
	public void newCampaign() {
		driver.get(WebDriverManager.TEST_URL + "/Login");
		WebDriverManager.overrideCertificate();
		LoginPage login = PageFactory.initElements(driver, LoginPage.class);
		login.loginAsAccount(accountEmail,accountPass);
		NewCampaignWizardPage page = PageFactory.initElements(driver, NewCampaignWizardPage.class);
		page.typeRss(campaignUrl);
		Assert.assertTrue("Next button should be enabled!", page.isNextEnabled());
		CampaignSettingsPage csp = page.clickNext();
		csp.typeName(cpName);
		csp.setAgeRestriction(CampaignSettingsPage.AgeRestriction.AGE_13);
		csp.setAgeRestriction(CampaignSettingsPage.AgeRestriction.AGE_18);
		csp.setAgeRestriction(CampaignSettingsPage.AgeRestriction.AGE_0);
		csp.confirmAgeRestriction();
		Assert.assertTrue("Next button should be enabled!", csp.isNextEnabled());
		CampaignSetBudgetPage csbp= csp.clickNext();
		csbp.typeCpc("0.1");
		csbp.typeBudget("25");
		Assert.assertTrue("Done button should be enabled!", csbp.isDoneEnabled());
		csbp.clickDone();
		

	}

}
